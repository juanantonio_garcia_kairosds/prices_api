package com.pruebatec.pricesapi.infrastructure.rest.spring.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.pruebatec.pricesapi.MappersConfig;

@SpringBootTest(classes = { MappersConfig.class })
@AutoConfigureMockMvc
class PriceControllerTest {

	private final static String REQUEST_URL = "/prices/price";

	private final static Long BRAND_ID = 1l;

	private final static Long PRODUCT_ID = 35455l;

	private final static String TEST_1_DATE = "2020-06-14-10.00.00";

	private final static String TEST_2_DATE = "2020-06-14-16.00.00";

	private final static String TEST_3_DATE = "2020-06-14-21.00.00";

	private final static String TEST_4_DATE = "2020-06-15-10.00.00";

	private final static String TEST_5_DATE = "2020-06-16-21.00.00";

	@Autowired
	private MockMvc mockMvc;

	@Test
	void test1() throws Exception {
		genericTest(BRAND_ID, PRODUCT_ID, TEST_1_DATE, 1);
	}

	@Test
	void test2() throws Exception {
		genericTest(BRAND_ID, PRODUCT_ID, TEST_2_DATE, 2);
	}

	@Test
	void test3() throws Exception {
		genericTest(BRAND_ID, PRODUCT_ID, TEST_3_DATE, 1);
	}

	@Test
	void test4() throws Exception {
		genericTest(BRAND_ID, PRODUCT_ID, TEST_4_DATE, 3);
	}

	@Test
	void test5() throws Exception {
		genericTest(BRAND_ID, PRODUCT_ID, TEST_5_DATE, 4);
	}

	private void genericTest(Long brandId, Long productId, String implementationDateStr, Integer expectedPriceList)
			throws Exception {
		RequestBuilder requestBuilder = testRequest(brandId, productId, implementationDateStr);
		mockMvc.perform(requestBuilder).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("priceList", is(expectedPriceList)));
	}

	private String generateJsonRequest(Long brandId, Long productId, String implementationDateStr) {
		return new StringBuilder("{\"brandId\":").append(brandId).append(",\"productId\":").append(productId)
				.append(",\"date\":\"").append(implementationDateStr).append("\"}").toString();
	}

	private RequestBuilder testRequest(Long brandId, Long productId, String implementationDateStr) throws Exception {
		String requestJson = generateJsonRequest(brandId, productId, implementationDateStr);
		return MockMvcRequestBuilders.get(REQUEST_URL).accept(MediaType.APPLICATION_JSON).content(requestJson)
				.contentType(MediaType.APPLICATION_JSON);
	}
}
