package com.pruebatec.pricesapi;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan("com.pruebatec.pricesapi.infrastructure")
public class MappersConfig {

}
