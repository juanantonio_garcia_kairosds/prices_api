package com.pruebatec.pricesapi.domain.service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.stereotype.Service;

import com.pruebatec.pricesapi.domain.model.Price;
import com.pruebatec.pricesapi.domain.repository.PriceRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PriceService {
	
	private final PriceRepository priceRepository;

	public Optional<Price> findPriorityPrice(LocalDateTime date, Long brandId, Long productId) {
		List<Price> prices = priceRepository.findAll();
		Predicate<Price> brandIdFilter = price -> price.getBrandId().equals(brandId);
		Predicate<Price> productIdFilter = price -> price.getProductId().equals(productId);
		Predicate<Price> dateFilter = price -> !date.isBefore(price.getStartDate())
				&& !date.isAfter(price.getEndDate());
		return prices.stream().filter(brandIdFilter.and(productIdFilter).and(dateFilter))
				.sorted(Comparator.comparing(Price::getPriority).reversed()).findFirst();
	}
}
