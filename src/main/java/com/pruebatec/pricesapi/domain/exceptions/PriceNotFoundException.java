package com.pruebatec.pricesapi.domain.exceptions;

public class PriceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -7325850623094572438L;

	public PriceNotFoundException() {
		super("Price not found");
	}

}
