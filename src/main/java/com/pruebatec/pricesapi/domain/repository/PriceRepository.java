package com.pruebatec.pricesapi.domain.repository;

import java.util.List;

import com.pruebatec.pricesapi.domain.model.Price;

public interface PriceRepository {

	List<Price> findAll();
}
