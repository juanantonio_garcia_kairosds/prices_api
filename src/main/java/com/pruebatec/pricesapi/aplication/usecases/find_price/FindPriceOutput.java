package com.pruebatec.pricesapi.aplication.usecases.find_price;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class FindPriceOutput {

	private Long productId;
	
	private Long brandId;
	
	private Integer priceList;

	private LocalDateTime startDate;
	
	private LocalDateTime endDate;
	
	private BigDecimal price;
	
	private String currency;
	
}
