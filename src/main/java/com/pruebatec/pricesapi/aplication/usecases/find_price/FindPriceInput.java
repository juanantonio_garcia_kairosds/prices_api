package com.pruebatec.pricesapi.aplication.usecases.find_price;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class FindPriceInput {

	private LocalDateTime date;

	private Long brandId;

	private Long productId;
}
