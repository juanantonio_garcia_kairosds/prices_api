package com.pruebatec.pricesapi.aplication.usecases.find_price;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pruebatec.pricesapi.domain.exceptions.PriceNotFoundException;
import com.pruebatec.pricesapi.domain.model.Price;
import com.pruebatec.pricesapi.domain.service.PriceService;
import com.pruebatec.pricesapi.infrastructure.rest.spring.mapper.PriceMapper;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class FindPriceUseCase {

	private final PriceService priceService;
	
	private final PriceMapper priceMapper;
	
	public FindPriceOutput execute(FindPriceInput input) {
		Optional<Price> optionalPrice = priceService.findPriorityPrice(input.getDate(), input.getBrandId(),
				input.getProductId());
		if (optionalPrice.isEmpty()) {
			throw new PriceNotFoundException();
		}
		return priceMapper.toFindPriceOutput(optionalPrice.get());
	}
}
