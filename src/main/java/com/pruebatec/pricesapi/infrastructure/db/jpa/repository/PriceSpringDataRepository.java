package com.pruebatec.pricesapi.infrastructure.db.jpa.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pruebatec.pricesapi.infrastructure.db.jpa.dbo.PriceEntity;

@Repository
public interface PriceSpringDataRepository extends JpaRepository<PriceEntity, UUID> {

}
