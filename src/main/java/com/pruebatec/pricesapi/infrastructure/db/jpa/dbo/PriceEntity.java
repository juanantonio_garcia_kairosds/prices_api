package com.pruebatec.pricesapi.infrastructure.db.jpa.dbo;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "PRICES")
public class PriceEntity {
	
	@Id
	@Column(name="ID_PRICES")
	private UUID priceId;
	
	@Column(name="BRAND_ID")
	private Long brandId;
	
	@Column(name="START_DATE")
	private LocalDateTime startDate;
	
	@Column(name="END_DATE")
	private LocalDateTime endDate;
	
	@Column(name="PRICE_LIST")
	private Integer priceList;
	
	@Column(name="PRODUCT_ID")
	private Long productId;
	
	@Column(name="PRIORITY")
	private Integer priority;
	
	@Column(name="PRICE")
	private BigDecimal value;
	
	@Column(name="CURR")
	private String currency;
	
	@Column(name="LAST_UPDATE")
	private LocalDateTime lastUpdate;
	
}
