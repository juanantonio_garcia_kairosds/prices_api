package com.pruebatec.pricesapi.infrastructure.db.jpa.repository;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pruebatec.pricesapi.domain.model.Price;
import com.pruebatec.pricesapi.domain.repository.PriceRepository;
import com.pruebatec.pricesapi.infrastructure.db.jpa.dbo.PriceEntity;
import com.pruebatec.pricesapi.infrastructure.db.jpa.mapper.PriceEntityMapper;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PriceDboRepository implements PriceRepository {

	private final PriceSpringDataRepository priceRepository;
	
	private final PriceEntityMapper priceMapper;

	@Override
	public List<Price> findAll() {
		List<PriceEntity> priceEntities = priceRepository.findAll();
		return priceEntities.stream().map(priceMapper::toDomain).collect(Collectors.toList());
	}

}
