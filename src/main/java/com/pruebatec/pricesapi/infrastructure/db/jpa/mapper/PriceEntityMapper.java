package com.pruebatec.pricesapi.infrastructure.db.jpa.mapper;

import org.mapstruct.Mapper;

import com.pruebatec.pricesapi.domain.model.Price;
import com.pruebatec.pricesapi.infrastructure.db.jpa.dbo.PriceEntity;

@Mapper(componentModel = "spring")
public interface PriceEntityMapper {

	Price toDomain(PriceEntity priceEntity);
	
	PriceEntity toDbo(Price price);
}
