package com.pruebatec.pricesapi.infrastructure.rest.spring.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.pruebatec.pricesapi.aplication.usecases.find_price.FindPriceOutput;
import com.pruebatec.pricesapi.domain.model.Price;

@Mapper(componentModel = "spring")
public interface PriceMapper {
	
	@Mapping(target="price", source="price.value")
	FindPriceOutput toFindPriceOutput(Price price);

}
