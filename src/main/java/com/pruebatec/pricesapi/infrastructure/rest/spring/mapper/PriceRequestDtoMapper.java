package com.pruebatec.pricesapi.infrastructure.rest.spring.mapper;

import org.mapstruct.Mapper;

import com.pruebatec.pricesapi.aplication.usecases.find_price.FindPriceInput;
import com.pruebatec.pricesapi.infrastructure.rest.spring.dto.PriceRequestDto;

@Mapper(componentModel = "spring")
public interface PriceRequestDtoMapper {
	
	FindPriceInput toFindPriceInput(PriceRequestDto priceRequestDto);

}
