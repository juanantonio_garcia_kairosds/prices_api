package com.pruebatec.pricesapi.infrastructure.rest.spring.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebatec.pricesapi.aplication.usecases.find_price.FindPriceOutput;
import com.pruebatec.pricesapi.aplication.usecases.find_price.FindPriceUseCase;
import com.pruebatec.pricesapi.infrastructure.rest.spring.dto.PriceRequestDto;
import com.pruebatec.pricesapi.infrastructure.rest.spring.mapper.PriceRequestDtoMapper;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/prices")
@RequiredArgsConstructor
public class PriceController {
	
	private final PriceRequestDtoMapper priceRequestDtoMapper;
	
	private final FindPriceUseCase findPriceUseCase;

	@GetMapping("/price")
	public ResponseEntity<FindPriceOutput> getPrice(@RequestBody PriceRequestDto requestDto) {
		return ResponseEntity.ok(findPriceUseCase.execute(priceRequestDtoMapper.toFindPriceInput(requestDto)));
	}
}
