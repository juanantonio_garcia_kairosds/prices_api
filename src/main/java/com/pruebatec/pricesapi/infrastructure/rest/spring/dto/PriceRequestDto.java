package com.pruebatec.pricesapi.infrastructure.rest.spring.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class PriceRequestDto {

	@JsonFormat(pattern = "yyyy-MM-dd-HH.mm.ss")
	private LocalDateTime date;
	
	private Long brandId;
	
	private Long productId;
}
