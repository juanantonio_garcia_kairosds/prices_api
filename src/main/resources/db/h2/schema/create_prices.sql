CREATE TABLE PRICES (
	ID_PRICES UUID NOT NULL PRIMARY KEY,
    BRAND_ID BIGINT NOT NULL, 
    START_DATE TIMESTAMP NOT NULL, 
    END_DATE TIMESTAMP NOT NULL,
    PRICE_LIST INTEGER NOT NULL, 
    PRODUCT_ID BIGINT NOT NULL, 
    PRIORITY INTEGER NOT NULL, 
    PRICE NUMERIC(20, 2) NOT NULL,
    CURR VARCHAR(10) NOT NULL,
    LAST_UPDATE TIMESTAMP NOT NULL
);
