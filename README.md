# Prices REST API

REST API project implemented with Spring Boot and dokerized.

## Run the app with Docker

Install docker and docker compose. After that, execute docker-compose:

	$> docker-compose up 

## Run tests with Maven

	mvn test -Dtest="com.pruebatec.pricesapi.infrastructure.rest.spring.controller.**"

# REST API usage

REST API endpoints are described below.

## Find price

### Request

`GET /prices/price`

### Request body JSON schema

	{
	  "brandId": { "type": "string" },
	  "productId": { "type": "string" },
	  "date": { "type": "string", "format": "yyyy-MM-dd-HH.mm.ss" }
	}
	
### Request example

	curl -i -X GET "http://localhost:8080/prices/price" -H "Content-Type: application/json" -d "{\"brandId\": 1,\"productId\": 35455,\"date\": \"2020-06-16-21.00.00\"}"

### Successful response

	HTTP/1.1 200
	Content-Type: application/json
	Transfer-Encoding: chunked
	Date: Thu, 22 Sep 2022 07:12:57 GMT
	
	{"productId":35455,"brandId":1,"priceList":4,"startDate":"2020-06-15T16:00:00","endDate":"2020-12-31T23:59:59","price":38.95,"currency":"EUR"}

### Not found price response

	HTTP/1.1 404
	Content-Type: text/plain;charset=UTF-8
	Content-Length: 15
	Date: Thu, 22 Sep 2022 07:14:23 GMT
	
	Price not found
